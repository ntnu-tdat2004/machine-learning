import numpy as np
import tensorflow as tf

class LinearRegressionModel:
    def __init__(self):
        # model input
        self.x = tf.placeholder(tf.float32)
        self.y = tf.placeholder(tf.float32)
        
        # model variables
        self.W = tf.Variable([[0.0]])
        self.b = tf.Variable([[0.0]])
    
        # predictor
        f = tf.matmul(self.x, self.W) + self.b
    
        # uses Mean Squared Error. The error function is also sometimes called cost or loss function
        self.error = tf.reduce_sum(tf.square(f - self.y))
model = LinearRegressionModel()

# observed/training input and output
x_train = np.mat([[1], [1.5], [2], [3], [4], [5],   [6]])
y_train = np.mat([[5], [3.5], [3], [4], [3], [1.5], [2]])

# training: adjust the model so that its error is minimized
minimize_operation = tf.train.GradientDescentOptimizer(0.01).minimize(model.error)

# create session object for running TensorFlow operations
session = tf.Session()

# initialize tf.Variable objects
session.run(tf.global_variables_initializer())

for epoch in range(1000):
    session.run(minimize_operation, {model.x: x_train, model.y: y_train})

# evaluate training accuracy
W, b, error  = session.run([model.W, model.b, model.error], {model.x: x_train, model.y: y_train})
print("W = %s, b = %s, error = %s" % (W, b, error))

session.close()
