import numpy as np

import matplotlib
matplotlib.use('TkAgg')

from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt

from matplotlib import cm
matplotlib.rcParams.update({'font.size': 11})

# regarding the notations, see http://stats.stackexchange.com/questions/193908/in-machine-learning-why-are-superscripts-used-instead-of-subscripts

W_init=np.mat([[-0.2]])
b_init=np.mat([[4.84]])
class LinearRegressionModel:    
    def __init__(self, W=W_init.copy(), b=b_init.copy()):
        self.W = W
        self.b = b
    
    # predictor
    def f(self, x): return x * self.W + self.b
    
    # uses Mean Squared Error. The error function is also sometimes called cost or loss function
    def error(self, x, y): return np.sum(np.power(self.f(x) - y, 2))
model = LinearRegressionModel()

# observed/training input and output
x_train = np.mat([[1], [1.5], [2], [3], [4], [5],   [6]])
y_train = np.mat([[5], [3.5], [3], [4], [3], [1.5], [2]])

fig = plt.figure("Linear regression: 2D")

plot1 = fig.add_subplot(121)

plot1.plot(x_train.A.squeeze(), y_train.A.squeeze(), 'o', label="$(\\hat x^{(i)},\\hat y^{(i)})$", color="blue")

plot1_f, = plot1.plot([[0], [10]], model.f(np.mat([[0], [10]])), color="green", label="$y = f(x) = xW+b$")

plot1_info = plot1.text(0.2, 0.6, "")

plot1_error=[]
for i in range(0, x_train.shape[0]):
    line, = plot1.plot([0, 0], [0, 0], color="red")
    plot1_error.append(line)
    if i == 0:
        line.set_label("$|f(\\hat x^{(i)})-\\hat y^{(i)}|$")

plot1.set_xlim(np.min(x_train[:,0]-1), np.max(x_train[:,0])+1)
plot1.set_ylim(np.min(y_train[:,0]-1), np.max(y_train[:,0])+2)
plot1.set_xlabel("$x$")
plot1.set_ylabel("$y$")
plot1.legend(loc="upper left")
plot1.set_xticks([])
plot1.set_yticks([])


plot2 = fig.add_subplot(122, projection='3d')

W_grid, b_grid = np.meshgrid(np.linspace(-0.55, -0.5, 20), np.linspace(4.7, 5, 20))
error_grid=np.empty([20, 20])
for i in range(0, W_grid.shape[0]):
    for j in range(0, W_grid.shape[1]):
        error_grid[i,j]=LinearRegressionModel(W_grid[i, j], b_grid[i, j]).error(x_train, y_train)
plot2.plot_surface(W_grid, b_grid, error_grid, cmap=cm.coolwarm)

plot2_error, = plot2.plot(model.W, model.b, model.error(x_train, y_train), 'o', color="purple")

plot2.set_xlabel("$W$")
plot2.set_ylabel("$b$")
plot2.set_zlabel("$error$")
plot2.set_xticks([])
plot2.set_yticks([])
plot2.set_zticks([])
plot2.set_xlim([-0.55, -0.5])
plot2.set_ylim([4.6, 5])
plot2.set_zlim([2.6, 2.75])

def update_figure(event=None):
    if(event is not None):
        if event.key == "W":
            model.W[0,0]+=0.01
        elif event.key == "w":
            model.W[0,0]-=0.01
        
        elif event.key == "B":
            model.b[0,0]+=0.05
        elif event.key == "b":
            model.b[0,0]-=0.05
        
        elif event.key == "c":
            model.W=W_init.copy()
            model.b=b_init.copy()

    plot1_f.set_data([0, 10], model.f(np.mat([[0], [10]])))
    
    for i in range(0, x_train.shape[0]):
        plot1_error[i].set_data([x_train[i,0], x_train[i,0]], [y_train[i,0], model.f(x_train[i,0])])
    
    plot1_info.set_text("$W=[%.2f]$\n$b=[%.2f]$\n$error = \\sum_i(f(\\hat x^{(i)}) - \\hat y^{(i)})^2 = %.2f$" % (model.W[0,0], model.b[0,0], model.error(x_train, y_train)))
    
    plot2_error.set_data(model.W, model.b)
    plot2_error.set_3d_properties(model.error(x_train, y_train))
    
    fig.canvas.draw()

update_figure()
fig.canvas.mpl_connect('key_press_event', update_figure)

plt.show()
