import numpy as np
import tensorflow as tf

class LogisticRegressionModel:
    def __init__(self):
        # model input
        self.x = tf.placeholder(tf.float32)
        self.y = tf.placeholder(tf.float32)
        
        # model variables
        self.W = tf.Variable([[0.1], [0.1]])
        self.b = tf.Variable([[0.1]])
    
        # predictor
        f = tf.sigmoid(tf.matmul(self.x, self.W) + self.b)
    
        # uses Cross Entropy. The error function is also sometimes called cost or loss function
        self.error = -tf.reduce_sum(self.y * tf.log(f) + (1 - self.y) * tf.log(1 - f))
        
        # the following function is equal to the function above, but is more numerically stable:
        # self.error = tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(labels=self.y, logits=tf.matmul(self.x, self.W) + self.b))
model = LogisticRegressionModel()

# observed/training input and output
x_train = np.mat([[0, 0], [0, 1], [1, 0], [1, 1]])
y_train = np.mat([[0],    [1],    [1],    [1]])

# training: adjust the model so that its error is minimized
minimize_operation = tf.train.GradientDescentOptimizer(0.01).minimize(model.error)

# create session object for running TensorFlow operations
session = tf.Session()

# initialize tf.Variable objects
session.run(tf.global_variables_initializer())

for epoch in range(25000):
    session.run(minimize_operation, {model.x: x_train, model.y: y_train})

# evaluate training accuracy
W, b, error  = session.run([model.W, model.b, model.error], {model.x: x_train, model.y: y_train})
print("W = %s, b = %s, error = %s" % (W, b, error))

session.close()
