import numpy as np
import tensorflow as tf

class LogisticRegressionModel:
    def __init__(self):
        # model input
        self.x = tf.placeholder(tf.float32)
        self.y = tf.placeholder(tf.float32)
        
        # model variables
        self.W1 = tf.Variable(tf.random_uniform([2,2], -1, 1))
        self.b1 = tf.Variable([[0.0, 0.0]])
        self.W2 = tf.Variable(tf.random_uniform([2,1], -1, 1))
        self.b2 = tf.Variable([[0.0]])
    
        # hidden layer
        h = tf.sigmoid(tf.matmul(self.x, self.W1) + self.b1)
    
        # predictor
        f = tf.sigmoid(tf.matmul(h, self.W2) + self.b2)
    
        # uses Cross Entropy. The error function is also sometimes called cost or loss function
        self.error = -tf.reduce_sum(self.y * tf.log(f) + (1 - self.y) * tf.log(1 - f))
        
        # the following function is equal to the function above, but is more numerically stable:
        # self.error = tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(labels=self.y, logits=tf.matmul(h, self.W) + self.b))
model = LogisticRegressionModel()

# observed/training input and output
x_train = np.mat([[0, 0], [0, 1], [1, 0], [1, 1]])
y_train = np.mat([[0],    [1],    [1],    [0]])

# training: adjust the model so that its error is minimized
minimize_operation = tf.train.GradientDescentOptimizer(0.01).minimize(model.error)

# create session object for running TensorFlow operations
session = tf.Session()

# initialize tf.Variable objects
session.run(tf.global_variables_initializer())

for epoch in range(100000):
    if epoch % 1000 == 0:
        print("epoch", epoch)
        print("error", session.run(model.error, {model.x: x_train, model.y: y_train}))
    session.run(minimize_operation, {model.x: x_train, model.y: y_train})

# evaluate training accuracy
W1, b1, W2, b2, error  = session.run([model.W1, model.b1, model.W2, model.b2, model.error], {model.x: x_train, model.y: y_train})
print("W1 = %s, \nb1 = %s, \nW2 = %s, b2 = %s, error = %s" % (W1, b1, W2, b2, error))

session.close()
