import numpy as np
import tensorflow as tf

from tensorflow.examples.tutorials.mnist import input_data

# Note that the MNIST data matrices are transposed compared to examples 1-4
class ConvolutionalNeuralNetworkModel:
    def __init__(self):
        # model input
        self.x = tf.placeholder(tf.float32)
        self.y = tf.placeholder(tf.float32)
        
        # model variables
        W1_conv = tf.Variable(tf.random_normal([5, 5, 1, 32]))
        b1_conv = tf.Variable(tf.random_normal([32]))
        W2_conv = tf.Variable(tf.random_normal([5, 5, 32, 64]))
        b2_conv = tf.Variable(tf.random_normal([64]))
        W3 = tf.Variable(tf.random_normal([7*7*64, 1024]))
        b3 = tf.Variable(tf.random_normal([1024]))
        W4 = tf.Variable(tf.random_normal([1024, 10]))
        b4 = tf.Variable(tf.random_normal([10]))
    
        x_reshaped = tf.reshape(self.x, shape=[-1, 28, 28, 1])
        
        f1_conv = tf.nn.bias_add(tf.nn.conv2d(x_reshaped, W1_conv, strides=[1, 1, 1, 1], padding='SAME'), b1_conv)
        max_pool1 = tf.nn.max_pool(f1_conv, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
        
        f2_conv = tf.nn.bias_add(tf.nn.conv2d(max_pool1, W2_conv, strides=[1, 1, 1, 1], padding='SAME'), b2_conv)
        max_pool2 = tf.nn.max_pool(f2_conv, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
    
        f3 = tf.nn.relu(tf.add(tf.matmul(tf.reshape(max_pool2, [-1, W3.get_shape().as_list()[0]]), W3), b3))
        
        # predictor, but without softmax (softmax is performed by the error function)
        f_without_softmax = tf.add(tf.matmul(f3, W4), b4)
    
        # uses multiclass Cross Entropy. The error function is also sometimes called cost or loss function
        self.error = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=self.y, logits=f_without_softmax))
        
        # accuracy
        self.accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(f_without_softmax, 1), tf.argmax(self.y, 1)), tf.float32))
model = ConvolutionalNeuralNetworkModel()

mnist = input_data.read_data_sets("/tmp/data/", one_hot=True)

minimize_operation = tf.train.AdamOptimizer(0.001).minimize(model.error)

# create session object for running TensorFlow operations
session = tf.Session()

# initialize tf.Variable objects
session.run(tf.global_variables_initializer())

batch_size=100
number_of_batches = mnist.train.num_examples/batch_size
for epoch in range(50):
    error_total=0
    for i in range(int(number_of_batches)):
        x_train, y_train = mnist.train.next_batch(batch_size)
        _, error = session.run([minimize_operation, model.error], {model.x: x_train, model.y: y_train})
        error_total+=error
    
    print("epoch", "%s" % epoch, "error", "%s" % (error_total/number_of_batches))

    if (epoch % 10) == 9:
        print("accuracy", session.run(model.accuracy, {model.x: mnist.test.images, model.y: mnist.test.labels}))

session.close()
