import numpy as np
import tensorflow as tf

class LongShortTermMemoryModel:
    def __init__(self, time_steps, vocab_size, batch_size, reuse_variables=False, scope_name="LongShortTermMemoryModel"):
        # model input
        self.x = tf.placeholder(tf.float32, [None, time_steps, vocab_size])
        self.y = tf.placeholder(tf.float32, [None, time_steps, vocab_size])
        
        rnn_size = 256
        # model variables
        with tf.variable_scope(scope_name) as scope:
            if reuse_variables:
                scope.reuse_variables()
            self.W = tf.get_variable("W", [rnn_size, vocab_size], initializer=tf.random_normal_initializer())
            self.b = tf.get_variable("b", [vocab_size], initializer=tf.random_normal_initializer())
    
        rnn_cell = tf.contrib.rnn.BasicLSTMCell(rnn_size)
        
        self.in_state = rnn_cell.zero_state(batch_size, tf.float32)
        
        x_list = tf.unstack(self.x, time_steps, 1)
        
        with tf.variable_scope(scope_name) as scope:
            if reuse_variables:
                scope.reuse_variables()
            self.out, self.out_state = tf.contrib.rnn.static_rnn(rnn_cell, x_list, initial_state=self.in_state, dtype=tf.float32)
    
        # predictor
        self.f_without_softmax = tf.matmul(tf.reshape(tf.concat(self.out, 1), [-1, rnn_size]), self.W) + self.b
    
        # uses averaged Cross Entropy. The error function is also sometimes called cost or loss function
        self.error = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=self.f_without_softmax, labels=self.y))

class Dataset:
    def __init__(self, input):
        self.input=input
        self.unique_elements=np.unique(list(self.input))
        self.element_to_index = dict()
        self.index_to_element = []
        for element in self.unique_elements:
            self.element_to_index[element]=len(self.element_to_index)
            self.index_to_element.append(element)
    
    def elements_to_one_hot(self, elements):
        one_hot=[]
        for element in elements:
            one_hot.append(np.eye(len(self.unique_elements))[self.element_to_index[element]])
        return np.mat(one_hot)
    
    def one_hot_to_element(self, one_hot):
        return self.index_to_element[one_hot.argmax()]

example = 2

if example==1:
    dataset = Dataset(" hello ")
elif example==2:
    dataset = Dataset(" 0&0=0, 0&1=0, 1&0=0, 1&1=1, 0|0=0, 0|1=1, 1|0=1, 1|1=1, ")
else:
    # note that larger texts should be split into batches
    dataset = Dataset("""

Itsy bitsy spider went up the water spout
Down came the rain 
And washed the spider out
Out came the sun and dried up all the rain
And the itsy bitsy spider went up the spout again

The teeny tiny spider went up the water spout
Down came the rain
And washed the spider out
Out came the sun and dried up all the rain
And the teeny tiny spider went up the spout again

The big humongous spider went up the water spout
Down came the rain
And washed the spider out
Out came the sun and dried up all the rain
And the big humongous spider went up the spout again

""")

# observed/training input and output
x_train = [dataset.elements_to_one_hot(dataset.input[0:-1])]
y_train = [dataset.elements_to_one_hot(dataset.input[1:])]

model = LongShortTermMemoryModel(len(dataset.input)-1, len(dataset.unique_elements), 1)
model_generate = LongShortTermMemoryModel(1, len(dataset.unique_elements), 1, True)

# training: adjust the model so that its error is minimized
minimize_operation = tf.train.RMSPropOptimizer(0.01).minimize(model.error)

# create session object for running TensorFlow operations
session = tf.Session()

# initialize tf.Variable objects
session.run(tf.global_variables_initializer())
# initialize model.in_state
state = zero_state = session.run(model.in_state)

def generate(state, initial_text, length):
    state_generate=state
    elements=""
    for element in initial_text:
        elements+=element
        y, state_generate=session.run([model_generate.f_without_softmax, model_generate.out_state], {model_generate.x: [dataset.elements_to_one_hot(element)], model_generate.in_state: state_generate})
    for i in range(0, length-1):
        elements+=dataset.one_hot_to_element(y)
        y, state_generate=session.run([model_generate.f_without_softmax, model_generate.out_state], {model_generate.x: [dataset.elements_to_one_hot(dataset.one_hot_to_element(y))], model_generate.in_state: state_generate})
    elements+=dataset.one_hot_to_element(y)
    return elements

for epoch in range(500):
    _, state = session.run([minimize_operation, model.out_state], {model.x: x_train, model.y: y_train, model.in_state: zero_state})
    
    if epoch % 10 == 9:
        print("epoch", epoch)
        print("error", session.run(model.error, {model.x: x_train, model.y: y_train, model.in_state: zero_state}))
        
        if example==1:
            print(generate(zero_state, " h", 30))
        elif example==2:
            print(generate(zero_state, "0&0", 2))
            print(generate(zero_state, "0&1", 2))
            print(generate(zero_state, "1&0", 2))
            print(generate(zero_state, "1&1", 2))
            print(generate(zero_state, "0|0", 2))
            print(generate(zero_state, "0|1", 2))
            print(generate(zero_state, "1|0", 2))
            print(generate(zero_state, "1|1", 2))
            print(generate(zero_state, "0=0=", 1))
            print(generate(zero_state, "1=1=", 1))
        else:
            print(generate(zero_state, "\n\n", 400))

session.close()
