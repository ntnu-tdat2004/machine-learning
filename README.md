# Visualizations and examples of various machine learning methods

## Dependencies
* Python, NumPy, matplotlib, TensorFlow, Tk

### Debian based distributions
```sh
sudo apt-get install python-matplotlib python-pip tk
sudo pip install tensorflow
```

### Arch Linux based distributions
```sh
sudo pacman -S python-matplotlib python-pip tk
sudo pip install tensorflow
```

### MacOS
```sh
brew install python matplotlib
pip install tensorflow
```

## Run
Start the various scripts by running:
```sh
python [script name]
```
